FROM pytorch/pytorch:latest

# Update system
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install zip curl python3-opencv -y
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash - && \
    apt-get install nodejs -y

# Install Python packages and jupyter extensions
RUN pip install --upgrade pip
RUN pip install jupyterlab ipywidgets \
    plotly matplotlib opencv-python \
    torchsummary scikit-learn-intelex \
    jupyterlab_code_formatter isort black

# Enable ToC
RUN jupyter labextension install @jupyterlab/toc

# Copy data
COPY ./data /app/data

# During development, the src folder will be overriden by a volume
COPY ./src /app/src

WORKDIR /app/src
CMD ["jupyter", "lab", "--port=8888", "--no-browser", \
     "--ip=0.0.0.0", "--allow-root"]
