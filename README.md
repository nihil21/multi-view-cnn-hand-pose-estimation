# 3D Hand Pose Estimation using Multi-View CNNs
In this project, I implement and train the MultiViewHandPoseCNN model from [Ge et al.](https://arxiv.org/abs/1606.07253) (2016) for 3D hand pose estimation on the [MSRA dataset](https://www.cv-foundation.org/openaccess/content_cvpr_2015/papers/Sun_Cascaded_Hand_Pose_2015_CVPR_paper.pdf) (2015).

The MSRA dataset can be downloaded using `wget -c https://www.dropbox.com/s/c91xvevra867m6t/cvpr15_MSRAHandGestureDB.zip`.

### Usage
This project relies on Docker: to build the image and start the container, it is sufficient to run `docker-compose up` in a terminal from the root directory: to access the Jupyter notebook, open a browser and go to `127.0.0.1:8888`.

To download the MSRA dataset you can run the `utils/download_msra_dataset.sh` script. To clean it from invalid samples (i.e., samples with less than 20 depth pixels, identified manually beforehand), you can run the python script `utils/clean_data.py`.

To enable GPU support, please make sure to have the Nvidia driver installed on your host, together with `nvidia-container-toolkit` and `nvidia-docker` (v2 or higher): follow [this link](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker) for more information.

### Author:
[Mattia Orlandi](https://github.com/nihil21)
