import glob
import os
import struct
import time
import typing as th

import numpy as np
import torch
import torchvision
from torch.utils.data import Dataset

from .plotting import display_image
from .transforms import (DepthToPointCloud, GenerateHeatmaps,
                         LocalContrastNormalization, ProjectPointCloud,
                         RandomRotate, ToTensor, WorldToOBBCoordinates)


def read_bin_to_depth(filename: str) -> np.ndarray:
    """Function that reads depth information from a binary file and reconstructs a NumPy's array.
    :param filename: the path to the binary file.

    :returns the depth image corresponding to the binary file."""

    with open(filename, "rb") as f:
        # Read first 24 bytes as 6 unsigned integers
        (
            img_width,
            img_height,
            left_offset,
            top_offset,
            right_offset,
            bottom_offset,
        ) = struct.unpack("<6I", f.read(24))
        img_shape = (img_height, img_width)
        roi_shape = (bottom_offset - top_offset, right_offset - left_offset)
        tot_val = roi_shape[0] * roi_shape[1]
        # Read the (4n) subsequent bytes as floats (n specified by the offsets) and convert the resulting
        # list into a NumPy's array
        depths_roi = struct.unpack(f"<{tot_val}f", f.read(tot_val * 4))
        depths_roi = np.array(depths_roi).reshape(roi_shape)
    # The file contains only depth information about the ROI (where the hand is),
    # so an array with the original shape must be created
    depth_image = np.zeros(img_shape)
    # Change ROI according to values read from file
    depth_image[top_offset:bottom_offset, left_offset:right_offset] = depths_roi
    return depth_image


class MSRADataset(Dataset):
    """Class extending the PyTorch's Dataset class that loads and preprocesses the MSRA dataset.

    The dataset must be structured as follows:
    Pi
    |- Gj
        |- joint.txt
        |- k_depth.bin
    where Pi is the i-th subject (9 in total), Gj is the j-th gesture (17 per subject), while k_depth.bin is the k-th
    binary file (around 500 per gesture); joint.txt is the file containing the 3 coordinates of each 21 hand joint,
    for every 500 binary file.
    By default, the __getitem__ method of this class simply loads a binary file and produces the related depth image,
    but it can be extended with preprocessing transforms from the hpe.transforms module via the add_transform method
    (there is a mandatory transform order which is documented directly in the add_transform method).
    The class also provides a method to cache preprocessed data by saving them to disk (cf. cache_to_disk method).

    Attributes:
        _root_folder: path to the root folder of the dataset;
        _cached: flag specifying whether the preprocessed data has been already cached or not;
        _data: nested dictionary containing all the samples grouped by subject and gesture;
        _transforms: list of preprocessing transforms."""

    def __init__(self, root_folder: str, cached: bool = False) -> None:
        super(MSRADataset, self).__init__()

        # Set root folder
        self._root_folder = root_folder
        # Set cache flag
        self._cached = cached

        # Create a nested dictionary that will contain the samples, grouped by subject and gesture
        self._data = {}
        # List of 21 joints
        joint_list = [
            "wrist",
            "index_mcp",
            "index_pip",
            "index_dip",
            "index_tip",
            "middle_mcp",
            "middle_pip",
            "middle_dip",
            "middle_tip",
            "ring_mcp",
            "ring_pip",
            "ring_dip",
            "ring_tip",
            "little_mcp",
            "little_pip",
            "little_dip",
            "little_tip",
            "thumb_mcp",
            "thumb_pip",
            "thumb_dip",
            "thumb_tip",
        ]

        start = time.time()

        # For each subject, read the hand poses folders
        subject_iter = sorted(glob.glob(os.path.join(root_folder, "P*")))
        for i, subject in enumerate(subject_iter):
            cur_subject = {}

            # For each gesture, save the path to each binary file
            gesture_iter = sorted(glob.glob(os.path.join(subject, "*")))
            for gesture in gesture_iter:

                # Read binary files
                bin_files = sorted(glob.glob(os.path.join(gesture, "*.bin")))
                # List of dictionaries joint <-> 3D coordinates
                joints = []
                # Parse joint.txt file and save 3D coordinates of joints
                with open(os.path.join(gesture, "joint.txt"), "r") as f:
                    lines = f.readlines()
                    lines.pop(0)  # discard first line
                    for line in lines:
                        # Split line and convert to floats
                        coords_list = map(float, line.strip().split())
                        # Group the list by (x, y, z) coordinates
                        coords_list = zip(*(iter(coords_list),) * 3)
                        # Create dictionary and append it to list
                        joints.append(
                            {
                                joint: np.array(coords)
                                for joint, coords in zip(joint_list, coords_list)
                            }
                        )

                if not self._cached:
                    # Add samples to dictionary for current subject
                    cur_subject[gesture.split("/")[-1]] = list(zip(bin_files, joints))

                else:  # store also the path to the already preprocessed .npz files
                    tr_data = sorted(glob.glob(os.path.join(gesture, "*_tr_data.npz")))
                    params = sorted(glob.glob(os.path.join(gesture, "*_params.npz")))

                    # Add samples to dictionary for current subject
                    cur_subject[gesture.split("/")[-1]] = list(
                        zip(bin_files, joints, tr_data, params)
                    )
            # Add dictionary for current subject to global dictionary
            self._data[subject.split("/")[-1]] = cur_subject

            print("\r", end="")
            print(f"Progress: {i + 1}/{len(subject_iter)}", end="", flush=True)

        elapsed = time.time() - start
        print(f" (elapsed: {elapsed:.1f} s)")

        # Initialize list for additional preprocessing steps
        self._transforms = []

    def __len__(self) -> int:
        return sum(sum(map(len, subject.values())) for subject in self._data.values())

    def __getitem__(
        self, idx: int
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray], torch.Tensor]]:
        # Define inner function to iterate over subjects and gestures and find the given index
        def find_index():
            item_count = 0
            # Iterate over subjects
            for sub, sub_data in self._data.items():
                sub_len = sum(map(len, sub_data.values()))
                if idx < item_count + sub_len:  # index in current subject
                    cur_sub = sub
                    # Iterate over gestures
                    for ges, ges_data in sub_data.items():
                        ges_len = len(ges_data)
                        if idx < item_count + ges_len:  # index in current gesture
                            cur_ges = ges
                            cur_it = idx - item_count
                            return cur_sub, cur_ges, cur_it
                        else:  # index not in current gesture
                            item_count += ges_len
                else:  # index not in current subject
                    item_count += sub_len
            raise IndexError(f"Index {idx} out of bounds.")

        cur_subject, cur_gesture, cur_item = find_index()
        sample_path = self._data[cur_subject][cur_gesture][cur_item]
        bin_file = sample_path[0]
        joints = sample_path[1]

        # Open and read the binary file to produce the actual depth image
        depth_image = read_bin_to_depth(bin_file)

        if not self._cached:
            sample = {"hand": depth_image, "joints": joints}
        else:
            # Read .npz files
            tr_data = np.load(sample_path[2])
            params = np.load(sample_path[3])
            # Extract hand and joints data
            hand_proj = tr_data["hand_proj"]
            joints_proj = tr_data["joints_proj"]
            # Extract params
            obb_len = params["obb_len"]
            trans_mtx = params["trans_mtx"]
            proj_params = params["proj_params"]

            sample = {
                "hand": depth_image,
                "joints": joints,
                "hand_proj": hand_proj,
                "joints_proj": joints_proj,
                "obb_len": obb_len,
                "trans_mtx": trans_mtx,
                "proj_params": proj_params,
            }
        # Apply additional transform
        for transform in self._transforms:
            sample = transform(sample)
        return sample

    def add_transform(
        self,
        transform: th.Union[
            DepthToPointCloud,
            WorldToOBBCoordinates,
            ProjectPointCloud,
            LocalContrastNormalization,
            GenerateHeatmaps,
            ToTensor,
            RandomRotate,
            torchvision.transforms.transforms.Compose,
        ],
    ) -> None:
        """Method to add preprocessing transforms; the order must be the following:
        - if data are not yet cached:
            1. DepthToPointCloud
            2. WorldToOBBCoordinates
            3. ProjectPointCloud
        - if data are already cached:
            1. a torchvision Compose of LocalContrastNormalization and GenerateHeatmaps
            2. a torchvision Compose of ToTensor and any data augmentation transform
        :param transform: the preprocessing transform."""
        self._transforms.append(transform)

    def remove_transform(self) -> None:
        """Method that removes the last preprocessing transform."""
        self._transforms.pop()

    def cache_to_disk(self) -> None:
        """Method that caches to disk in .npz compressed format the following data:
        - the hand point cloud 96x96 projections;
        - the projections of joints;
        - the OBB dimensions
        - the transformation matrix;
        - the OBB parameters.
        This method should be called after adding the ProjectPointCloud transform."""
        if self._cached:
            print("Dataset already cached to disk.")
        else:
            # Indexes
            t_idx = 0  # total samples index
            s_idx = 0  # subject index

            start = time.time()

            # Iterate over subjects
            while s_idx < len(self._data.keys()):
                sub, sub_data = list(self._data.items())[s_idx]

                g_idx = 0  # gesture index
                # Iterate over gestures
                while g_idx < len(sub_data.keys()):
                    ges, ges_data = list(self._data[sub].items())[g_idx]
                    cur_folder = os.path.join(self._root_folder, sub, ges)

                    i_idx = 0  # item index
                    # Iterate over items
                    while i_idx < len(ges_data):
                        # Get sample
                        sample = self[t_idx]
                        # Display image
                        hand_projections = np.hstack(
                            [sample["hand_proj"][i] for i in range(3)]
                        )
                        display_image(hand_projections)

                        # Write hand and joints projections to compressed .npz file
                        np.savez_compressed(
                            os.path.join(cur_folder, f"{i_idx:06d}_tr_data"),
                            hand_proj=sample["hand_proj"],
                            joints_proj=sample["joints_proj"],
                        )
                        # Write remaining parameters to compressed .npz file
                        obb_len = [
                            sample["obb"][4, 0] - sample["obb"][0, 0],
                            sample["obb"][2, 1] - sample["obb"][0, 1],
                            sample["obb"][1, 2] - sample["obb"][0, 2],
                        ]
                        np.savez_compressed(
                            os.path.join(cur_folder, f"{i_idx:06d}_params"),
                            obb_len=obb_len,
                            trans_mtx=sample["trans_mtx"],
                            proj_params=sample["proj_params"],
                        )

                        # Update total samples index
                        t_idx += 1
                        print("\r", end="")
                        print(f"Progress: {t_idx}/{len(self)}", end="", flush=True)

                        # Update item index
                        i_idx += 1
                    # Update gesture index
                    g_idx += 1
                # Update subject index
                s_idx += 1

            elapsed = time.time() - start
            print(f" (elapsed: {elapsed:.1f} s)")

            # Set cache flag and drop all transforms
            self._cached = True
            self._transforms = []

    def get_subjects(self) -> th.List[str]:
        """Method that returns the list of subjects.

        :returns the list of subjects"""
        return list(self._data.keys())

    def get_subject_indexes(self, subject: str):
        """Method that returns all the indexes of samples associated to a given subject.
        :param subject: string representing a subject.

        :returns list of indexes associated to the subject."""
        offset = sum(
            map(
                len,
                [
                    ges_data
                    for sub in self._data.keys()
                    for ges_data in self._data[sub].values()
                    if sub < subject
                ],
            )
        )
        sub_n_samples = sum(map(len, self._data[subject].values()))

        return [i + offset for i in range(sub_n_samples)]
