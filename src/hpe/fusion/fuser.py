import glob
import os
import typing as th
from warnings import warn

import cv2
import numpy as np
import torch
from sklearn.decomposition import PCA

from .utils import to_3d, uv2xy, yz2uv, zx2uv


class GaussFailError(Exception):
    pass


class Fuser:

    """Callable class implementing the multiview fusion step, which consists in:
    - estimating a 3D Gaussian distribution from the 3 heatmaps returned by the CNN;
    - constraining the optimal locations to be in a low dimensional subspace generated by
    a PCA (performed on the training joints);
    - back-projecting the optimal locations into the original joints space.

    Attributes:
        _m: integer representing the dimension of the subspace (must be lower than 3 * n_joints);
        _proj_res: integer representing the resolution of the (squared) hand projections;
        _heat_res: integer representing the resolution of the (squared) joints heatmaps;
        _n_joints: number of joints;
        _eig_vecs: NumPy's 2D array in which each row represents an eigenvector (ordered by relevance);
        _eig_vals: NumPy's 1D array in which each element represents an eigenvalue (ordered by relevance);
        _mean_vec: NumPy's 1D array representing the mean coordinates of each joint."""

    def __init__(
        self,
        m: int,
        proj_res: int,
        heat_res: int,
        subjects: th.List[str],
        data_folder: str,
    ) -> None:
        self._m = m
        self._proj_res = proj_res
        self._heat_res = heat_res

        # Initially set number of joints and PCA data to None
        self._n_joints = None
        self._eig_vecs = None
        self._eig_vals = None
        self._mean_vec = None
        # Compute joints PCA
        self._compute_joints_pca(subjects, data_folder)

    def _compute_joints_pca(self, subjects: th.List[str], data_folder: str) -> None:
        """Method that computes the PCA of the joints of the given subjects.
        :param subjects: list of subjects to include in the PCA computation;
        :param data_folder: path to the folder containing subjects' data."""
        joints = []

        # For each subject, read the hand poses folders
        for subject in subjects:
            sub_path = os.path.join(data_folder, subject)

            # For each gesture, read the .txt file with joints data
            gesture_iter = sorted(glob.glob(os.path.join(sub_path, "*")))
            for gesture in gesture_iter:
                with open(os.path.join(gesture, "joint.txt")) as f:
                    lines = f.readlines()
                    lines.pop(0)  # discard first line
                    for line in lines:
                        # Split line and convert to floats
                        coords_list = map(float, line.strip().split())
                        # Group the list by (x, y, z) coordinates
                        coords_list = list(zip(*(iter(coords_list),) * 3))
                        # Create array and append it to list
                        joints.append(np.array(coords_list).flatten())
        joints = np.array(joints)  # shape: (n_samples, n_joints * 3)
        self._n_joints = joints.shape[1] // 3

        # Perform PCA on all joints_eig_vecs
        pca = PCA()
        pca.fit(joints)

        # Obtain eigenvectors
        self._eig_vecs = pca.components_
        # Obtain eigenvalues
        self._eig_vals = pca.explained_variance_
        # Compute mean vector
        self._mean_vec = joints.mean(axis=0)

    def __call__(
        self,
        sample: th.Dict[
            str, th.Union[np.ndarray, th.Dict[str, np.ndarray], torch.Tensor]
        ],
    ) -> np.ndarray:
        # Extract predictions
        pred_heat = sample["pred_heat"]
        # Extract transformation matrix
        trans_mtx = sample["trans_mtx"]
        # Scale projection parameters according to heatmap resolution
        proj_params = np.copy(sample["proj_params"])
        proj_params[:, :4] = proj_params[:, :4] * self._heat_res / self._proj_res
        # Extract OBB dimensions
        obb_len = sample["obb_len"]

        # Convert PCA from world to OBB coordinates
        eig_vecs, mean_vec = self._pca_to_obb_coordinates(trans_mtx)

        # 1. Compute mean and covariance for each joint point

        # Output arrays for mean and inverse covariance
        joints_mean = np.empty(shape=(self._n_joints, 3), dtype=np.float)
        joints_inv_cov = np.empty(shape=(self._n_joints, 3, 3), dtype=np.float)

        k = (self._proj_res / self._heat_res) ** 2  # scaling factor for covariance
        # Iterate over joints
        cur_joint = 0
        while cur_joint < self._n_joints:
            try:
                (cur_joint_mean, cur_joint_cov,) = self._estimate_gaussian(
                    pred_heat[:, cur_joint], proj_params, z_len=obb_len[2]
                )

                # Convert mean from HEAT_RESxHEAT_RES space to PROJ_RESxPROJ_RES
                joints_mean[cur_joint] = self._to_proj_res(
                    cur_joint_mean,
                    obb_len=obb_len,
                )

                # Convert covariance from HEAT_RESxHEAT_RES space to
                # PROJ_RESxPROJ_RES and invert it
                joints_inv_cov[cur_joint] = np.linalg.inv(cur_joint_cov * k)
            except GaussFailError:
                break

            # Increment joint index
            cur_joint += 1

        if cur_joint < self._n_joints:  # estimation failed for some joints
            # joint_name = list(sample["joints"].keys())[cur_joint]
            # warn(f"INFO: Gaussian estimation failed for the {joint_name} joint.")
            return self._fuse_sub(pred_heat, trans_mtx, proj_params, obb_len)

        # 2. A * alpha = b

        # Compute matrix A
        a_mtx = np.zeros(shape=(self._m, self._m), dtype=np.float)
        for cur_joint in range(self._n_joints):
            # (3, 3) * (M, 3).T -> (3, M)
            tmp = joints_inv_cov[cur_joint] @ eig_vecs[:, cur_joint].T
            # (M, 3) * (3, M) -> (M, M)
            a_mtx += eig_vecs[:, cur_joint] @ tmp

        # Compute vector b
        b_vec = np.zeros(shape=(self._m,), dtype=np.float)
        for cur_joint in range(self._n_joints):
            # (3, 3) * (M, 3).T -> (3, M)
            tmp = joints_inv_cov[cur_joint] @ eig_vecs[:, cur_joint].T
            # (3,) * (3, M) -> (M,)
            b_vec += (joints_mean[cur_joint] - mean_vec[cur_joint]) @ tmp

        # Compute vector alpha: (M, M) * (M,) = (M,)
        alpha_vec = np.linalg.inv(a_mtx) @ b_vec

        # 3. Recover from PCA

        estimated_points = np.einsum("m, mjk -> jk", alpha_vec, eig_vecs) + mean_vec

        # 4. Reconvert estimated points from OBB to world coordinates

        # Convert estimated_points to homogeneous coordinates
        estimated_points = np.hstack([estimated_points, np.ones((self._n_joints, 1))])
        # Change coordinate system
        estimated_points = (trans_mtx @ estimated_points.T).T
        # Convert back to 3D coordinates
        estimated_points = estimated_points[:, :3]

        return estimated_points

    def _pca_to_obb_coordinates(
        self, trans_mtx: np.ndarray
    ) -> th.Tuple[np.ndarray, np.ndarray]:
        """Method that changes the reference system of PCA eigenvectors and mean vector according to
        the given OBB transformation matrix.
        :param trans_mtx: NumPy's 4x4 array representing the OBB transformation matrix in
        homogeneous coordinates.

        :returns the first M PCA eigenvectors in the OBB coordinate system;
        :returns the mean vector in the OBB coordinate system."""
        # Invert transformation matrix
        inv_trans_mtx = np.linalg.inv(trans_mtx)

        # Change coordinate system of the first m eigenvectors
        eig_vecs = np.empty(shape=(self._m, self._n_joints, 3), dtype=np.float)
        for i_joint in range(self._n_joints):
            idx = i_joint * 3
            cur_eig_vecs = self._eig_vecs[: self._m, idx:idx + 3]
            # Convert to homogeneous coordinates
            cur_eig_vecs = np.hstack([cur_eig_vecs, np.ones((self._m, 1))])
            # Multiply by transformation matrix
            cur_eig_vecs = (inv_trans_mtx @ cur_eig_vecs.T).T
            # Convert back to 3D coordinates
            eig_vecs[:, i_joint] = cur_eig_vecs[:, :3]

        # Change coordinate system of the mean vector
        mean_vec = np.empty(shape=(self._n_joints, 3), dtype=np.float)
        for i_joint in range(self._n_joints):
            idx = i_joint * 3
            cur_mean_vec = self._mean_vec[idx:idx + 3].reshape(1, -1)
            # Convert to homogeneous coordinates
            cur_mean_vec = np.hstack([cur_mean_vec, np.ones((1, 1))])
            # Multiply by transformation matrix
            cur_mean_vec = (inv_trans_mtx @ cur_mean_vec.T).T
            # Convert back to 3D coordinates
            mean_vec[i_joint] = cur_mean_vec[:, :3]

        return eig_vecs, mean_vec

    def _estimate_gaussian(
        self, joint_heat: np.ndarray, proj_params: np.ndarray, z_len: np.ndarray
    ) -> th.Tuple[np.ndarray, np.ndarray]:
        """Method that, given the XY, YZ and ZX heatmaps of a joint, tries to estimate
        the mean and covariance of a 3D Gaussian distribution.
        :param joint_heat: NumPy's 3xHEAT_RESxHEAT_RES array representing the three heatmaps;
        :param proj_params: NumPy's 3x5 array representing the projection parameters of the current sample;
        :param z_len: float representing the Z dimension of current sample's OBB.

        :returns the estimated mean of the 3D Gaussian distribution for the current joint;
        :returns the estimated covariance of the 3D Gaussian distribution for the current joint."""

        # 1. Binarize predicted heatmaps

        heat_binary = np.empty(shape=joint_heat.shape, dtype=np.uint8)
        # Convert predictions to 8bit images
        xy8bit = (joint_heat[0] * 256).astype(np.uint8)
        yz8bit = (joint_heat[1] * 256).astype(np.uint8)
        zx8bit = (joint_heat[2] * 256).astype(np.uint8)
        # Apply Otsu's thresholding to obtain binary images (joint/not joint)
        _, heat_binary[0] = cv2.threshold(
            xy8bit, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )
        _, heat_binary[1] = cv2.threshold(
            yz8bit, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )
        _, heat_binary[2] = cv2.threshold(
            zx8bit, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )

        # 2. Get sample points

        # Output lists
        samples = []
        weights = []

        # Create array with depth levels
        z_range = np.arange(self._heat_res)
        z_val = z_len * (z_range + 0.5) / self._proj_res

        # Iterate over (u, v) coordinates of non-zero XY heatmap pixels
        # (i.e. locations containing predicted joints)
        v_coords, u_coords = np.nonzero(heat_binary[0])
        for u, v in list(zip(u_coords, v_coords)):
            # Compute X and Y from (u, v)
            x, y = uv2xy(u, v, proj_params)
            conf_xy = joint_heat[0, v, u]  # set confidence to XY heatmap intensity

            for i_z in range(self._heat_res):
                # Re-compute U and V from (y, z)
                u_yz, v_yz = yz2uv(y, z_val[i_z], proj_params)
                if (
                    u_yz < 0
                    or u_yz >= self._heat_res
                    or v_yz < 0
                    or v_yz >= self._heat_res
                    or heat_binary[1, v_yz, u_yz] == 0
                ):
                    continue
                conf_yz = joint_heat[
                    1, v_yz, u_yz
                ]  # set confidence to YZ heatmap intensity

                # Re-compute U and V from (z, x)
                u_zx, v_zx = zx2uv(z_val[i_z], x, proj_params)
                if (
                    u_zx < 0
                    or u_zx >= self._heat_res
                    or v_zx < 0
                    or v_zx >= self._heat_res
                    or heat_binary[2, v_zx, u_zx] == 0
                ):
                    continue
                conf_zx = joint_heat[
                    2, v_zx, u_zx
                ]  # set confidence to ZX heatmap intensity

                samples.append(np.array([x, y, z_val[i_z]]))
                weights.append(np.prod([conf_xy, conf_yz, conf_zx]))

        if len(samples) <= 1:  # at least 2 samples
            raise GaussFailError()

        # Convert samples and weights to arrays
        samples = np.array(samples)
        weights = np.array(weights)

        # 3. Get mean (weighted by confidence)

        joints_mean = np.average(samples, axis=0, weights=weights)

        # 4. Get covariance (weighted by confidence)

        joints_covariance = np.cov(
            samples - joints_mean, aweights=weights, rowvar=False
        )

        for i in range(3):
            joints_covariance[i, i] = max(joints_covariance[i, i], 1e-3)

        # Check determinant
        if np.linalg.det(joints_covariance) <= 1:
            raise GaussFailError()

        return joints_mean, joints_covariance

    def _fuse_sub(
        self,
        pred_heat: np.ndarray,
        trans_mtx: np.ndarray,
        proj_params: np.ndarray,
        obb_len: np.ndarray,
    ) -> np.ndarray:
        """Method that performs the multi-view fusion step via direct estimation of
        joints' coordinates, rather than using a Gaussian estimation.
        :param pred_heat: NumPy's 3xN_JOINTSxHEAT_RESxHEAT_RES array containing the
        3 predicted heatmaps for each joint;
        :param trans_mtx: NumPy's 4x4 array representing the OBB transformation matrix in
        homogeneous coordinates;
        :param proj_params: NumPy's 3x5 array representing the projection parameters of the current sample;
        :param obb_len: NumPy's array containing the OBB dimensions.

        :returns a NumPy's N_JOINTSx3 array representing the estimated 3D location of each joint."""

        # 1. Estimate joints 3D coordinates

        estimated_points = np.empty(shape=(self._n_joints, 3), dtype=np.float)

        for cur_joint in range(self._n_joints):
            estimated_points[cur_joint] = self._estimate_joint_xyz(
                pred_heat[:, cur_joint], proj_params, obb_len
            )

        # 2. Reconvert estimated points from OBB to world coordinates

        # Convert estimated_points to homogeneous coordinates
        estimated_points = np.hstack([estimated_points, np.ones((self._n_joints, 1))])
        # Change coordinate system
        estimated_points = (trans_mtx @ estimated_points.T).T
        # Convert back to 3D coordinates
        estimated_points = estimated_points[:, :3]

        return estimated_points

    def _estimate_joint_xyz(
        self, joint_heat: np.ndarray, proj_params: np.ndarray, obb_len: np.ndarray
    ) -> np.ndarray:
        """Method that, given the XY, YZ and ZX heatmaps of a joint, tries to directly
        estimate its 3D coordinates.
        :param joint_heat: NumPy's 3xHEAT_RESxHEAT_RES array representing the three heatmaps;
        :param proj_params: NumPy's 3x5 array representing the projection parameters of the current sample;
        :param obb_len: NumPy's array containing the OBB dimensions.

        :returns the estimated 3D coordinates of the joint."""
        # Estimation parameters
        h = 10
        h_2 = h ** 2
        threshold = 1e-5

        # 1. Binarize predicted heatmaps

        heat_binary = np.empty(shape=joint_heat.shape, dtype=np.uint8)
        # Convert predictions to 8bit images
        xy8bit = (joint_heat[0] * 256).astype(np.uint8)
        yz8bit = (joint_heat[1] * 256).astype(np.uint8)
        zx8bit = (joint_heat[2] * 256).astype(np.uint8)
        # Apply Otsu's thresholding to obtain binary images (joint/not joint)
        _, heat_binary[0] = cv2.threshold(
            xy8bit, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )
        _, heat_binary[1] = cv2.threshold(
            yz8bit, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )
        _, heat_binary[2] = cv2.threshold(
            zx8bit, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )

        # 2. Estimate 3D points

        pts_weights = [[] for _ in range(3)]
        # Iterate over projections
        for p in range(3):
            # Iterate over (u, v) coordinates of non-zero heatmap pixels
            # (i.e. locations containing predicted joints)
            v_coords, u_coords = np.nonzero(heat_binary[p])
            for u, v in list(zip(u_coords, v_coords)):
                # Compute X and Y from (u, v)
                x, y = to_3d(u, v, projection=p, proj_params=proj_params)
                z = joint_heat[p, v, u]  # set Z to heatmap value

                pts_weights[p].append(np.array([x, y, z]))

        cur_pt = np.zeros(shape=(3,), dtype=np.float)
        while True:  # progressively refine estimated point
            pre_pt = cur_pt

            x_num1 = y_num1 = x_den1 = y_den1 = 0.0
            y_num2 = z_num2 = y_den2 = z_den2 = 0.0
            z_num3 = x_num3 = z_den3 = x_den3 = 0.0

            for pts_weight in pts_weights[0]:  # XY
                exp_term = np.exp(
                    -(
                        (cur_pt[0] - pts_weight[0]) ** 2
                        + (cur_pt[1] - pts_weight[1]) ** 2
                    )
                    / h_2
                )
                tmp = pts_weight[2] * exp_term
                x_num1 += tmp * pts_weight[0]
                y_num1 += tmp * pts_weight[1]
                x_den1 += tmp
                y_den1 += tmp

            for pts_weight in pts_weights[1]:  # YZ
                exp_term = np.exp(
                    -(
                        (cur_pt[1] - pts_weight[0]) ** 2
                        + (cur_pt[2] - pts_weight[1]) ** 2
                    )
                    / h_2
                )
                tmp = pts_weight[2] * exp_term
                y_num2 += tmp * pts_weight[0]
                z_num2 += tmp * pts_weight[1]
                y_den2 += tmp
                z_den2 += tmp

            for pts_weight in pts_weights[2]:  # ZX
                exp_term = np.exp(
                    -(
                        (cur_pt[2] - pts_weight[0]) ** 2
                        + (cur_pt[0] - pts_weight[1]) ** 2
                    )
                    / h_2
                )
                tmp = pts_weight[2] * exp_term
                z_num3 += tmp * pts_weight[0]
                x_num3 += tmp * pts_weight[1]
                z_den3 += tmp
                x_den3 += tmp

            # Update current estimation
            cur_pt[0] = (x_num1 + x_num3) / (x_den1 + x_den3)
            cur_pt[1] = (y_num1 + y_num2) / (y_den1 + y_den2)
            cur_pt[2] = (z_num2 + z_num3) / (z_den2 + z_den3)

            # Stop when the norm of the difference between
            # the current and previous estimates is below threshold
            diff = pre_pt - cur_pt
            if np.linalg.norm(diff) < threshold:
                break

        # Convert points from HEAT_RESxHEAT_RES space to PROJ_RESxPROJ_RES
        cur_pt = self._to_proj_res(cur_pt, obb_len)

        return cur_pt

    def _to_proj_res(self, xyz: np.array, obb_len: np.ndarray) -> np.ndarray:
        """Method that converts points from the HEAT_RESxHEAT_RES space to the PROJ_RESxPROJ_RES space.
        :param xyz: NumPy's array representing the set of 3D points to be converted;
        :param obb_len: NumPy's array containing the OBB dimensions.

        :returns the transformed set of points."""
        xyz_proj = np.empty(shape=(3,), dtype=np.float)

        xyz_proj[0] = (self._proj_res * xyz[0]) / self._heat_res - obb_len[0] / 2.0
        xyz_proj[1] = -(self._proj_res * xyz[1]) / self._heat_res + obb_len[1] / 2.0
        xyz_proj[2] = (self._proj_res * xyz[2]) / self._heat_res - obb_len[2] / 2.0

        return xyz_proj
