import typing as th

import numpy as np


def uv2xy(u: int, v: int, proj_params: np.ndarray) -> th.Tuple[float, float]:
    # Get params
    proj_x = proj_params[:, 0]
    proj_y = proj_params[:, 1]
    proj_scale = proj_params[:, 4]
    # Compute X and Y
    x = (u - proj_x[0]) / proj_scale[0]
    y = (v - proj_y[0]) / proj_scale[0]
    return x, y


def uv2yz(u: int, v: int, proj_params: np.ndarray) -> th.Tuple[float, float]:
    # Get params
    proj_x = proj_params[:, 0]
    proj_y = proj_params[:, 1]
    proj_scale = proj_params[:, 4]
    # Compute Y and Z
    y = (u - proj_x[1]) / proj_scale[1]
    z = (v - proj_y[1]) / proj_scale[1]
    return y, z


def uv2zx(u: int, v: int, proj_params: np.ndarray) -> th.Tuple[float, float]:
    # Get params
    proj_x = proj_params[:, 0]
    proj_y = proj_params[:, 1]
    proj_scale = proj_params[:, 4]
    # Compute Z and X
    z = (u - proj_x[2]) / proj_scale[2]
    x = (v - proj_y[2]) / proj_scale[2]
    return z, x


def xy2uv(x: float, y: float, proj_params: np.ndarray) -> th.Tuple[int, int]:
    # Get params
    proj_x = proj_params[:, 0]
    proj_y = proj_params[:, 1]
    proj_scale = proj_params[:, 4]
    # Compute U and V
    u = round(proj_scale[0] * x + proj_x[0])
    v = round(proj_scale[0] * y + proj_y[0])
    return u, v


def yz2uv(y: float, z: float, proj_params: np.ndarray) -> th.Tuple[int, int]:
    # Get params
    proj_x = proj_params[:, 0]
    proj_y = proj_params[:, 1]
    proj_scale = proj_params[:, 4]
    # Compute U and V
    u = round(proj_scale[1] * y + proj_x[1])
    v = round(proj_scale[1] * z + proj_y[1])
    return u, v


def zx2uv(z: float, x: float, proj_params: np.ndarray) -> th.Tuple[int, int]:
    # Get params
    proj_x = proj_params[:, 0]
    proj_y = proj_params[:, 1]
    proj_scale = proj_params[:, 4]
    # Compute U and V
    u = round(proj_scale[2] * z + proj_x[2])
    v = round(proj_scale[2] * x + proj_y[2])
    return u, v


def to_3d(
    u: int, v: int, projection: int, proj_params: np.ndarray
) -> th.Tuple[float, float]:
    if projection == 0:  # XY
        return uv2xy(u, v, proj_params)
    elif projection == 1:  # YZ
        return uv2yz(u, v, proj_params)
    elif projection == 2:  # ZX
        return uv2zx(u, v, proj_params)
    else:
        raise IndexError("The 'projection' parameter must be either 0, 1 or 2.")
