import typing as th

import torch
import torch.nn as nn
import torch.nn.functional as F


class MVCNNBranch(nn.Module):
    def __init__(self):
        super(MVCNNBranch, self).__init__()
        # Two-staged CNN branches, one for each resolution
        self.fine_branch = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=4),
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=(6, 6)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
        )
        self.middle_branch = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=(5, 5)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
        )
        self.coarse_branch = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=(4, 4)),
            nn.ReLU(),
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=(4, 4)),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
        )
        # Final linear layers
        self.fc = nn.Sequential(
            nn.Linear(in_features=7776, out_features=6804),
            nn.ReLU(),
            nn.Linear(in_features=6804, out_features=6804),
            nn.ReLU(),
        )

    def forward(self, x: torch.FloatTensor) -> torch.Tensor:
        batch_size = x.shape[0]
        # Downsample to 24x24 and 48x48
        x_fine = x  # finer scale is 96x96, as the original
        x_middle = F.interpolate(x, size=48)
        x_coarse = F.interpolate(x, size=24)
        # Process each image separately
        x_fine = self.fine_branch(x_fine)
        x_middle = self.middle_branch(x_middle)
        x_coarse = self.coarse_branch(x_coarse)
        # Concatenate output feature maps
        x = torch.cat([x_fine, x_middle, x_coarse], 1)
        # Flatten tensor
        x = x.view(batch_size, -1)
        # Apply linear layers
        x = self.fc(x)
        # Final reshape into (batch_size, 21, 18, 18)
        return x.reshape(batch_size, 21, 18, 18)


class MVCNN(nn.Module):
    def __init__(
        self,
        xy_branch: th.Optional[MVCNNBranch] = None,
        yz_branch: th.Optional[MVCNNBranch] = None,
        zx_branch: th.Optional[MVCNNBranch] = None,
    ):
        super(MVCNN, self).__init__()
        self.xy_branch = xy_branch if xy_branch is not None else MVCNNBranch()
        self.yz_branch = yz_branch if yz_branch is not None else MVCNNBranch()
        self.zx_branch = zx_branch if zx_branch is not None else MVCNNBranch()

    def forward(
        self, projections: torch.FloatTensor
    ) -> th.Tuple[torch.FloatTensor, torch.FloatTensor, torch.FloatTensor]:
        # Input tensor represents a batch of point clouds' projections
        # with shape [batch_size, 3, 96, 96]
        # Take the three projections
        xy_proj = projections[:, 0, :, :].unsqueeze(1)
        yz_proj = projections[:, 1, :, :].unsqueeze(1)
        zx_proj = projections[:, 2, :, :].unsqueeze(1)
        # Feed the projections to the three branches
        xy_heatmap = self.xy_branch(xy_proj)
        yz_heatmap = self.yz_branch(yz_proj)
        zx_heatmap = self.zx_branch(zx_proj)

        # Stack the 3 heatmaps into a single tensor
        return torch.stack([xy_heatmap, yz_heatmap, zx_heatmap], dim=1)

    def predict(self, projections: torch.FloatTensor) -> torch.FloatTensor:
        if len(projections.shape) == 3:  # add batch dimension
            projections = projections.unsqueeze(0)

        with torch.no_grad():
            # Feed the projections to the network
            heatmaps = self(projections)
            # Normalize them such that their sum is 1
            heatmaps /= heatmaps.sum(dim=(3, 4), keepdim=True)
        return heatmaps.squeeze(0)
