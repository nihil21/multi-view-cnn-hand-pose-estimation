import time
import typing as th

import numpy as np
import torch
import torch.nn as nn
from torch.optim.optimizer import Optimizer
from torch.utils.data import DataLoader

from ..nn import MVCNN, MVCNNBranch


# Function computing the mean of a sequence
def mean(lst: th.Sequence[float]) -> float:
    return sum(lst) / len(lst)


def hyperparameter_tuning(
    train_data: DataLoader,
    val_data: DataLoader,
    criterion: nn.MSELoss,
    part_opts: th.Sequence[th.Callable[[th.Iterator[torch.nn.Parameter]], Optimizer]],
    epochs: int,
    device: torch.device,
) -> th.Callable[[], Optimizer]:
    opt_dict = {}

    start = time.time()

    # Iterate over partial optimizers
    for part_opt in part_opts:
        # Create model and optimizer
        model = MVCNN().to(device)
        opt = part_opt(model.parameters())

        # Train model
        history = training_loop(
            model,
            train_data,
            criterion,
            opt,
            epochs,
            device,
            val_data=val_data,
            use_amp=True,
            verbose=False,
        )

        model.cpu()

        # Get the index associated with the lowest validation loss
        min_val_loss_idx = np.argmin(history["val_loss"])
        # Get minimum validation loss
        min_val_loss = history["val_loss"][min_val_loss_idx]

        print(f"Optimizer: {part_opt} -> val_loss: {min_val_loss:e}")

        opt_dict[part_opt] = min_val_loss

    best_opt = min(opt_dict.items(), key=lambda t: t[1])[0]

    elapsed = time.time() - start
    print(
        f"Best optimizer: {best_opt} - Validation loss: {opt_dict[best_opt]:e} (elapsed: {elapsed:.3f} s)"
    )

    return best_opt


def train(
    model: th.Union[MVCNN, MVCNNBranch],
    data: DataLoader,
    criterion: nn.MSELoss,
    optimizer: Optimizer,
    device: torch.device,
    projection: th.Optional[int] = None,
    scaler: th.Optional[torch.cuda.amp.GradScaler] = None,
    verbose: bool = True,
) -> float:
    """Function that trains a given model for one epoch.
    :param model: instance of MVCNN or MVCNNBranch;
    :param data: instance of DataLoader with the training data;
    :param criterion: regression loss (e.g. MSE) with either standard or custom reduction;
    :param optimizer: optimizer algorithm;
    :param device: device on which the training will be performed;
    :param projection: integer from 0 to 2 representing the projection on which the training
                       will be done (optional, relevant only if the model is MVCNNBranch);
    :param scaler: optional GradScaler instance to enable AMP;
    :param verbose: optional bool specifying whether to print logs to standard output (true by default).

    :returns the training loss averaged over all the samples."""
    loss_data = []

    # Activate train mode
    model.train()

    n_samples = len(data)
    for i, sample_batched in enumerate(data):
        optimizer.zero_grad()
        # Extract hand projections and joints heatmaps
        batch_proj, batch_heats = (
            sample_batched["hand_proj"],
            sample_batched["joints_proj"],
        )
        if isinstance(model, MVCNN):
            # Move tensors to GPU
            batch_proj = batch_proj.to(device)
            batch_heats = batch_heats.to(device)

            if scaler is None:
                # Make prediction
                pred_heats = model(batch_proj)
                # Compute loss
                loss = criterion(pred_heats, batch_heats)
            else:
                with torch.cuda.amp.autocast():
                    # Make prediction
                    pred_heats = model(batch_proj)
                    # Compute loss
                    loss = criterion(pred_heats, batch_heats)

            # Move tensors back to CPU
            batch_proj.cpu()
            batch_heats.cpu()

            # Sum loss along projection dimension:
            # (batch_size, 3, n_joint, res, res) -> (batch_size, n_joint, res, res)
            loss = loss.sum(dim=1)

        elif isinstance(model, MVCNNBranch) and projection is not None:
            # Extract ground truth heat map:
            # (batch_size, 3, n_joint, res, res) -> 3 x (batch_size, n_joints, res, res)
            batch_heats = batch_heats[:, projection]
            # Move tensors to GPU
            batch_proj = batch_proj[:, projection].unsqueeze(1)
            batch_proj = batch_proj.to(device)
            batch_heats = batch_heats.to(device)

            if scaler is None:
                # Make prediction
                pred_heats = model(batch_proj)
                # Compute loss
                loss = criterion(pred_heats, batch_heats)
            else:
                with torch.cuda.amp.autocast():
                    # Make prediction
                    pred_heats = model(batch_proj)
                    # Compute loss
                    loss = criterion(pred_heats, batch_heats)

            # Move tensors back to CPU
            batch_proj.cpu()
            batch_heats.cpu()
        else:
            raise NotImplementedError(
                "This function is supported only for MVCNN and MVCNNBranch."
            )

        # Reduce loss:
        # 1. Sum along heatmap dimensions and joint dimension.
        # 2. Average along batch dimension.
        if criterion.reduction == "none":
            loss = loss.sum(dim=(1, 2, 3)).mean()

        # Backpropagation
        if scaler is None:
            loss.backward()
            optimizer.step()
        else:
            scaler.scale(loss).backward()
            scaler.step(optimizer)
            scaler.update()

        # Update history
        loss_value = loss.item()
        loss_data.append(loss_value)

        if verbose:
            f_i = str(i + 1)
            lead_zeros = len(str(n_samples)) - len(f_i)
            f_i = "".join(["0"] * lead_zeros + list(f_i))
            print("\r", end="")
            print(
                f"Progress: {f_i}/{n_samples} | Loss: {loss_value:e}\t",
                end="",
                flush=True,
            )

    return mean(loss_data)


def evaluate(
    model: th.Union[MVCNN, MVCNNBranch],
    data: DataLoader,
    criterion: nn.MSELoss,
    device: torch.device,
    projection: th.Optional[int] = None,
    scaler: th.Optional[torch.cuda.amp.GradScaler] = None,
    verbose: bool = True,
) -> float:
    """Function that evaluates a given model.
    :param model: instance of MVCNN or MVCNNBranch;
    :param data: instance of DataLoader with the validation data;
    :param criterion: regression loss (e.g. MSE) with either standard or custom reduction;
    :param device: device on which the training will be performed;
    :param projection: integer from 0 to 2 representing the projection on which the evaluation
                       will be done (optional, relevant only if the model is MVCNNBranch);
    :param scaler: optional GradScaler instance to enable AMP;
    :param verbose: optional bool specifying whether to print logs to standard output (true by default).

    :returns the validation loss averaged over all the samples."""
    loss_data = []

    # Activate eval mode
    model.eval()

    with torch.no_grad():
        n_samples = len(data)
        for i, sample_batched in enumerate(data):
            # Extract hand projections and joints heatmaps
            batch_proj, batch_heats = (
                sample_batched["hand_proj"],
                sample_batched["joints_proj"],
            )
            if isinstance(model, MVCNN):
                # Move tensors to GPU
                batch_proj = batch_proj.to(device)
                batch_heats = batch_heats.to(device)

                if scaler is None:
                    # Make prediction
                    pred_heats = model(batch_proj)
                    # Compute loss
                    loss = criterion(pred_heats, batch_heats)
                else:
                    with torch.cuda.amp.autocast():
                        # Make prediction
                        pred_heats = model(batch_proj)
                        # Compute loss
                        loss = criterion(pred_heats, batch_heats)

                # Move tensors back to CPU
                batch_proj.cpu()
                batch_heats.cpu()

                # Sum loss along projection dimension:
                # (batch_size, 3, n_joint, res, res) -> (batch_size, n_joint, res, res)
                loss = loss.sum(dim=1)

            elif isinstance(model, MVCNNBranch) and projection is not None:
                # Extract ground truth heat map:
                # (batch_size, 3, n_joint, res, res) -> 3 x (batch_size, n_joints, res, res)
                batch_heats = batch_heats[:, projection]
                # Move tensors to GPU
                batch_proj = batch_proj[:, projection].unsqueeze(1)
                batch_proj = batch_proj.to(device)
                batch_heats = batch_heats.to(device)

                if scaler is None:
                    # Make prediction
                    pred_heats = model(batch_proj)
                    # Compute loss
                    loss = criterion(pred_heats, batch_heats)
                else:
                    with torch.cuda.amp.autocast():
                        # Make prediction
                        pred_heats = model(batch_proj)
                        # Compute loss
                        loss = criterion(pred_heats, batch_heats)

                # Move tensors back to CPU
                batch_proj.cpu()
                batch_heats.cpu()
            else:
                raise NotImplementedError(
                    "This function is supported only for MVCNN and MVCNNBranch."
                )

            # Reduce loss:
            # 1. Sum along heatmap dimensions and joint dimension.
            # 2. Average along batch dimension.
            if criterion.reduction == "none":
                loss = loss.sum(dim=(1, 2, 3)).mean()

            # Update history
            loss_value = loss.item()
            loss_data.append(loss_value)

            if verbose:
                f_i = str(i + 1)
                lead_zeros = len(str(n_samples)) - len(f_i)
                f_i = "".join(["0"] * lead_zeros + list(f_i))
                print("\r", end="")
                print(
                    f"Progress: {f_i}/{n_samples} | Loss: {loss_value:e}\t\t",
                    end="",
                    flush=True,
                )

    return mean(loss_data)


def training_loop(
    model: th.Union[MVCNN, MVCNNBranch],
    train_data: DataLoader,
    criterion: nn.MSELoss,
    optimizer: Optimizer,
    epochs: int,
    device: torch.device,
    checkpoint_path: th.Optional[str] = None,
    val_data: th.Optional[DataLoader] = None,
    projection: th.Optional[int] = None,
    use_amp: bool = False,
    early_stopping: bool = False,
    patience: int = 5,
    tolerance: float = 1e-4,
    verbose: bool = True,
) -> th.Dict[str, th.List[float]]:
    """Function performing training and evaluation of a given model.
    :param model: instance of MVCNN or MVCNNBranch;
    :param train_data: instance of DataLoader with the training data;
    :param criterion: regression loss (e.g. MSE) with either standard or custom reduction;
    :param optimizer: optimizer algorithm;
    :param epochs: number of epochs;
    :param device: device on which the training will be performed;
    :param checkpoint_path: path to the .pt file in which the model will be saved (optional);
    :param val_data: instance of DataLoader with the validation data (optional);
    :param projection: integer from 0 to 2 representing the projection on which the training
                       will be done (optional, relevant only if the model is MVCNNBranch);
    :param use_amp: boolean flag specifying whether to use AMP (false by default);
    :param early_stopping: boolean flag specifying whether to employ early stopping on the validation loss
                           (false by default, only relevant if both val_data and checkpoint_path are specified);
    :param patience: maximum number of epochs that early stopping waits when there's no improvement
                     in validation loss (5 by default);
    :param tolerance: minimum required improvement for the validation loss (1e-4 by default);
    :param verbose: optional bool specifying whether to print logs to standard output (true by default)."""
    history = {"loss": [], "val_loss": []}

    # Initialize variables for early stopping
    min_val_loss = np.inf
    no_improve_counter = 0

    # Create GradScaler
    scaler = torch.cuda.amp.GradScaler() if use_amp else None

    for ep in range(epochs):
        if verbose:
            print("-" * 100)
            print(f"Epoch {ep + 1}/{epochs}")
            print("> Training:")

        start = time.time()
        train_loss = train(
            model,
            train_data,
            criterion,
            optimizer,
            device,
            projection,
            scaler,
            verbose=verbose,
        )
        elapsed = time.time() - start
        history["loss"].append(train_loss)

        if verbose:
            print(f" (elapsed: {elapsed:.1f} s)")

        # Do validation if required
        if val_data is not None:
            if verbose:
                print("> Validation:")

            start = time.time()
            val_loss = evaluate(
                model, val_data, criterion, device, projection, scaler, verbose=verbose
            )
            elapsed = time.time() - start
            history["val_loss"].append(val_loss)

            if verbose:
                print(f" (elapsed: {elapsed:.1f} s)")
                print(f"Results: train_loss = {train_loss:e}, val_loss = {val_loss:e}")

            if early_stopping and checkpoint_path:
                # If validation loss is lower than minimum, update minimum
                if val_loss < min_val_loss - tolerance:
                    min_val_loss = val_loss
                    no_improve_counter = 0

                    if verbose:
                        print(f"INFO: new minimum val loss = {val_loss:e}")

                    # Save model
                    torch.save(model.state_dict(), checkpoint_path)
                # otherwise increment counter
                else:
                    no_improve_counter += 1
                # If loss did not improve for 'patience' epochs, break
                if no_improve_counter == patience:
                    if verbose:
                        print(
                            f"INFO: no improvement in validation loss for {patience} epochs from {min_val_loss:e}"
                        )

                    # Restore model to best
                    model.load_state_dict(torch.load(checkpoint_path))
                    break
        else:
            if verbose:
                print(f"Results: train_loss = {train_loss:e}")

    # Save model
    if checkpoint_path:
        torch.save(model.state_dict(), checkpoint_path)
        model.eval()

    return history
