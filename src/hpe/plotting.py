import typing as th

import cv2
import IPython.display as display
import numpy as np
import plotly.graph_objects as go
from matplotlib import pyplot as plt
from PIL import Image


def plot_hand_point_cloud(
    hand_pc: np.ndarray,
    ax: plt.Axes,
    title: str,
    joints: th.Optional[th.Dict[str, th.Tuple[float, float, float]]] = None,
) -> None:
    """Protected function that plots a single point cloud in 3D space (non-interactive).
    :param hand_pc: a (num_points, 3) NumPy's array representing the hand point cloud;
    :param ax: PyPlot's Axes object in which the point cloud will be plotted;
    :param title: the title of the plot;
    :param joints: optional dictionary which maps every joint with the respective 3D coordinates
                   (if specified, joints are plotted)."""

    # Compute boundaries
    x_lim = (min(hand_pc[:, 0]) - 10, max(hand_pc[:, 0]) + 10)
    y_lim = (min(hand_pc[:, 1]) - 10, max(hand_pc[:, 1]) + 10)
    z_lim = (min(hand_pc[:, 2]) - 10, max(hand_pc[:, 2]) + 10)
    # Set title, boundaries and view's angle
    ax.set_title(title, size=40)
    ax.set_xlim3d(*x_lim)
    ax.set_xlabel("X", labelpad=20, fontsize=20)
    ax.set_ylim3d(*y_lim)
    ax.set_ylabel("Y", labelpad=20, fontsize=20)
    ax.set_zlim3d(*z_lim)
    ax.set_zlabel("Z", labelpad=20, fontsize=20)
    ax.tick_params(axis="both", labelsize=15)
    ax.elev = 105
    ax.azim = -90
    size = 1 if joints is not None else 10
    # Scatter plot the point cloud
    ax.scatter(
        xs=hand_pc[:, 0],
        ys=hand_pc[:, 1],
        zs=hand_pc[:, 2],
        s=size,
        c=hand_pc[:, 2],
    )
    # Scatter plot the joints, if required
    if joints is not None:
        # Plot joint points
        for joint, coords in joints.items():
            ax.scatter(xs=coords[0], ys=coords[1], zs=coords[2], s=50, label=joint)
        # Plot joint segments
        fingers = ["thumb", "index", "middle", "ring", "little"]
        for finger in fingers:
            # Wrist to every MCP
            x, y, z = tuple(zip(joints["wrist"], joints[f"{finger}_mcp"]))
            ax.plot(x, y, z, color="b")
            # Every MCP to the respective PIP
            x, y, z = tuple(zip(joints[f"{finger}_mcp"], joints[f"{finger}_pip"]))
            ax.plot(x, y, z, color="b")
            # Every PIP to the respective DIP
            x, y, z = tuple(zip(joints[f"{finger}_pip"], joints[f"{finger}_dip"]))
            ax.plot(x, y, z, color="b")
            # Every DIP to the respective TIP
            x, y, z = tuple(zip(joints[f"{finger}_dip"], joints[f"{finger}_tip"]))
            ax.plot(x, y, z, color="b")
        ax.legend(loc="upper left", fontsize=20)


def plot_hands_point_clouds(
    hand_pc_list: th.List[np.ndarray],
    joints_list: th.Optional[th.List[th.Dict[str, np.ndarray]]] = None,
    fig_size: th.Optional[th.Tuple[int, int]] = None,
) -> None:
    """Function that plots a list of point clouds by iteratively calling plot_point_cloud().
    :param hand_pc_list: list of (num_points, 3) NumPy's array, each one representing a hand point cloud;
    :param joints_list: optional list of dictionaries, which maps every joint with the respective
                        3D coordinates (if specified, joints are plotted);
    :param fig_size: figure size."""

    fig = plt.figure(figsize=fig_size)
    # Compute number of rows and cols of subplots
    n_pcs = len(hand_pc_list)
    n_cols = 3
    n_rows = n_pcs // n_cols + 1
    if joints_list is None:
        joints_list = [None] * len(hand_pc_list)
    for i, (point_cloud, joints) in enumerate(zip(hand_pc_list, joints_list)):
        ax = fig.add_subplot(n_rows, n_cols, i + 1, projection="3d")
        plot_hand_point_cloud(point_cloud, ax, title=f"PC{i + 1}", joints=joints)
    fig.tight_layout()


def plot_hand_point_cloud_3d(
    hand_pc: np.ndarray,
    joints: th.Optional[th.Dict[str, np.ndarray]] = None,
    obb: th.Optional[np.ndarray] = None,
) -> None:
    """Function that creates an interactive 3D plot of a given hand point cloud and, optionally,
    it's OBB and the coordinates of joints.
    :param hand_pc: a (num_points, 3) NumPy's array representing a point cloud;
    :param obb: an optional (8, 3) NumPy's array representing the 8 vertices of the point cloud's OBB;
    :param joints: an optional dictionary associating to each joint a (3,) NumPy's array representing its coordinates.
    """
    # Draw hand point cloud
    fig = go.Figure(
        data=[
            go.Scatter3d(
                x=hand_pc[:, 0],
                y=hand_pc[:, 1],
                z=hand_pc[:, 2],
                mode="markers",
                name="hand",
                marker={"size": 2, "color": hand_pc[:, 2]},
            ),
        ],
        layout=go.Layout(
            autosize=False,
            width=1400,
            height=800,
        ),
    )

    # Draw joint segments
    if joints is not None:
        fingers = ["thumb", "index", "middle", "ring", "little"]
        for finger in fingers:
            # Wrist to every MCP
            seg = np.vstack([joints["wrist"], joints[f"{finger}_mcp"]])
            fig.add_trace(
                go.Scatter3d(
                    x=seg[:, 0],
                    y=seg[:, 1],
                    z=seg[:, 2],
                    mode="lines+markers",
                    name=f"wrist->{finger}_mcp",
                    marker={"size": 5},
                )
            )
            # Every MCP to the respective PIP
            seg = np.vstack([joints[f"{finger}_mcp"], joints[f"{finger}_pip"]])
            fig.add_trace(
                go.Scatter3d(
                    x=seg[:, 0],
                    y=seg[:, 1],
                    z=seg[:, 2],
                    mode="lines+markers",
                    name=f"{finger}_mcp->{finger}_pip",
                    marker={"size": 5},
                )
            )
            # Every PIP to the respective DIP
            seg = np.vstack([joints[f"{finger}_pip"], joints[f"{finger}_dip"]])
            fig.add_trace(
                go.Scatter3d(
                    x=seg[:, 0],
                    y=seg[:, 1],
                    z=seg[:, 2],
                    mode="lines+markers",
                    name=f"{finger}_pip->{finger}_dip",
                    marker={"size": 5},
                )
            )
            # Every DIP to the respective TIP
            seg = np.vstack([joints[f"{finger}_dip"], joints[f"{finger}_tip"]])
            fig.add_trace(
                go.Scatter3d(
                    x=seg[:, 0],
                    y=seg[:, 1],
                    z=seg[:, 2],
                    mode="lines+markers",
                    name=f"{finger}_dip->{finger}_tip",
                    marker={"size": 5},
                )
            )
    # Draw OBB
    if obb is not None:
        fig.add_trace(
            go.Mesh3d(
                x=obb[:8, 0],
                y=obb[:8, 1],
                z=obb[:8, 2],
                i=[0, 1, 0, 1, 4, 5, 6, 7, 0, 2, 1, 3],
                j=[1, 2, 1, 4, 5, 6, 7, 2, 2, 4, 3, 5],
                k=[2, 3, 4, 5, 6, 7, 2, 3, 4, 6, 5, 7],
                opacity=0.2,
                color="#DC143C",
                flatshading=True,
                name="obb",
            )
        )

    fig.show()


def plot_hand_projections(
    hand_proj: np.ndarray,
    joints_proj: th.Optional[np.ndarray] = None,
    negative: bool = False,
    color: bool = False,
) -> None:
    """Function that, given an array of projections (one per channel, in the CHW order) and (optionally) an
    array of joints' 2D coordinates, plots each projection with a circle around every joint.
    :param hand_proj: array of projections with shape (num_proj, height, width);
    :param joints_proj: optional array of joints' coordinates with shape (num_proj, num_joints, 2);
    :param negative: flag specifying whether to show the negative of the image or not (False by default);
    :param color: flag specifying whether to use a color map or not (False by default)."""
    fig = plt.figure(figsize=(30, 20))
    proj_dict = {0: "XY", 1: "YZ", 2: "ZX"}

    for i in range(3):
        if negative:
            hand_proj[i] = 1 - hand_proj[i]

        if joints_proj is not None:
            # Draw circle around joints
            for j in range(joints_proj.shape[1]):
                hand_proj[i] = cv2.circle(
                    hand_proj[i],
                    center=joints_proj[i, j],
                    radius=3,
                    color=(1.0, 1.0, 1.0),
                )

        ax = fig.add_subplot(1, 3, i + 1)
        ax.set_title(f"{proj_dict[i]} projection", size=20)
        ax.imshow(hand_proj[i], cmap="gray" if not color else None)
    fig.tight_layout()


def display_image(array: np.ndarray) -> None:
    """Function that converts a single-channel, FP32 NumPy's array into a PIL image and displays it.
    :param array: FP32 array with shape (height, width)."""
    array = (255 * array).astype("uint8")
    display.clear_output(
        wait=True
    )  # if called repeatedly, the function will display the new image in place
    display.display(Image.fromarray(array))


def plot_blended(hand_proj: np.ndarray, joints_heat: np.ndarray) -> None:
    fig = plt.figure(figsize=(30, 20))
    proj_dict = {0: "XY", 1: "YZ", 2: "ZX"}

    for i in range(3):
        # Normalize heatmap between 0 and 1, and resize it to match the hand projection
        joint_heat = cv2.normalize(
            joints_heat[i], dst=None, alpha=0.0, beta=1.0, norm_type=cv2.NORM_MINMAX
        )
        joint_heat = cv2.resize(np.amax(joint_heat, axis=0), hand_proj[i].shape)

        # Blend images
        blended = cv2.addWeighted(
            src1=hand_proj[i],
            alpha=0.5,
            src2=joint_heat,
            beta=0.5,
            gamma=0.0,
        )

        ax = fig.add_subplot(1, 3, i + 1)
        ax.set_title(f"{proj_dict[i]} projection", size=20)
        ax.imshow(blended)
    fig.tight_layout()


def plot_training_hist(
    history: th.Dict[str, th.List[float]],
    title: str,
    fig_size: th.Optional[th.Tuple[int, int]] = None,
) -> None:
    _, ax = plt.subplots(figsize=fig_size)
    ep_range = np.arange(1, len(history["loss"]) + 1)
    ax.plot(ep_range, history["loss"], label="train_loss")
    ax.plot(ep_range, history["val_loss"], label="val_loss")
    ax.set_xlabel("No. epochs")
    ax.set_ylabel("MSE")
    ax.set_title(title, fontsize=20)
    # ax.set_xticks(ep_range)
    ax.legend()
    ax.grid()
