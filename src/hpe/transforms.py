import typing as th
import warnings

import cv2
import numpy as np
import torch
from sklearn.decomposition import PCA


class DepthToPointCloud:
    """Callable class that, given camera's principal point and focal length,
    turns a depth image into a point cloud.

    Attributes:
        _principal_point: tuple of integers representing the principal point of the camera;
        _focal_length: float representing the focal length of the camera."""

    def __init__(
        self,
        principal_point: th.Tuple[int, int],
        focal_length: float,
    ) -> None:
        self._principal_point = principal_point
        self._focal_length = focal_length

    def __call__(
        self,
        sample: th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]]:
        # Extract depth image from sample
        depth_image = sample["hand"]

        # Transform depth image in point cloud
        rows, cols = depth_image.shape
        c, r = np.meshgrid(np.arange(cols), np.arange(rows), sparse=True)
        x = depth_image * (c - self._principal_point[0]) / self._focal_length
        y = -depth_image * (r - self._principal_point[1]) / self._focal_length
        z = -depth_image
        # Stack x, y and z
        point_cloud = np.dstack((x, y, z))
        # Remove points with zero depth, i.e. in (0, 0, 0)
        point_cloud = point_cloud[
            ~np.all(point_cloud == 0, axis=2)
        ]  # it will flatten the first two dimensions

        # Update dictionary and return it
        sample["hand"] = point_cloud
        return sample


class WorldToOBBCoordinates:
    """Callable class that, given a point cloud:
    - computes its 3 principal components;
    - roto-translates the point cloud in order to align it with the principal components."""

    def __call__(
        self,
        sample: th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]]:
        # Extract hand point cloud and joints from sample
        hand_pc = sample["hand"]
        joints = sample["joints"]

        # Compute center point
        center_pt = hand_pc.mean(axis=0)

        # Perform PCA and obtain eigenvectors
        pca = PCA()
        pca.fit(hand_pc)
        eig_mtx = pca.components_

        # Obtain X, Y and Z axes from eigenvectors
        if any(np.linalg.norm(eig_mtx[i]) == 0 for i in range(3)):
            warnings.warn("WARNING: one of the axis has zero norm.")
            x_axis = np.array([1.0, 0.0, 0.0])
            y_axis = np.array([0.0, 1.0, 0.0])
            z_axis = np.array([0.0, 0.0, 1.0])
        else:
            x_axis = eig_mtx[0]
            y_axis = eig_mtx[1]
            z_axis = np.cross(x_axis, y_axis)  # compute Z axis as cross product
            # Change orientation according to the sign of axes
            if z_axis[2] < 0:
                z_axis *= -1
                if x_axis[1] < 0:  # set X axis upward
                    x_axis = np.cross(y_axis, z_axis)
                else:
                    y_axis = np.cross(z_axis, x_axis)

        # Center point cloud and project it onto the three axes
        centered_pc = hand_pc - center_pt
        x_proj = centered_pc.dot(x_axis)
        y_proj = centered_pc.dot(y_axis)
        z_proj = centered_pc.dot(z_axis)
        # Compute projections' boundaries
        x_min, x_max = x_proj.min(), x_proj.max()
        y_min, y_max = y_proj.min(), y_proj.max()
        z_min, z_max = z_proj.min(), z_proj.max()
        # Adjust center point
        scaled_x_axis = x_axis * (x_min + x_max) / 2
        scaled_y_axis = y_axis * (y_min + y_max) / 2
        scaled_z_axis = z_axis * (z_min + z_max) / 2
        center_pt += scaled_x_axis + scaled_y_axis + scaled_z_axis
        # Compute OBB dimensions
        pad = 1.02
        obb_x_len = (x_max - x_min) * pad
        obb_y_len = (y_max - y_min) * pad
        obb_z_len = (z_max - z_min) * pad
        x_pad = (obb_x_len - x_max + x_min) / 2
        y_pad = (obb_y_len - y_max + y_min) / 2
        z_pad = (obb_z_len - z_max + z_min) / 2

        # Compute transformation matrix (in homogeneous coordinates)
        trans_mtx = np.vstack(
            [
                np.hstack(
                    [
                        x_axis.reshape(3, 1),
                        y_axis.reshape(3, 1),
                        z_axis.reshape(3, 1),
                        center_pt.reshape(3, 1),
                    ]
                ),
                np.array([0.0, 0.0, 0.0, 1]),
            ]
        )
        inv_trans_mtx = np.linalg.inv(trans_mtx)

        # Convert point cloud to homogeneous coordinates
        hand_pc = np.hstack([hand_pc, np.ones((hand_pc.shape[0], 1))])
        # Change coordinate system
        hand_pc = (inv_trans_mtx @ hand_pc.T).T
        # Convert back to 3D coordinates
        hand_pc = hand_pc[:, :3]

        # Convert joints to homogeneous coordinates
        joints_pc = np.array(list(joints.values()))
        joints_pc = np.hstack([joints_pc, np.ones((joints_pc.shape[0], 1))])
        # Change coordinate system
        joints_pc = (inv_trans_mtx @ joints_pc.T).T
        # Convert back to 3D coordinates
        joints_pc = joints_pc[:, :3]
        joints = {j: c for j, c in zip(joints.keys(), joints_pc)}

        # Compute OBB vertices
        x_offset = hand_pc[:, 0].min() - x_pad
        y_offset = hand_pc[:, 1].min() - y_pad
        z_offset = hand_pc[:, 2].min() - z_pad
        obb = np.array(
            [
                [x_offset, y_offset, z_offset],
                [x_offset, y_offset, z_offset + obb_z_len],
                [x_offset, y_offset + obb_y_len, z_offset],
                [x_offset, y_offset + obb_y_len, z_offset + obb_z_len],
                [x_offset + obb_x_len, y_offset, z_offset],
                [x_offset + obb_x_len, y_offset, z_offset + obb_z_len],
                [x_offset + obb_x_len, y_offset + obb_y_len, z_offset],
                [x_offset + obb_x_len, y_offset + obb_y_len, z_offset + obb_z_len],
            ]
        )

        # Update dictionary (include OBB and transformation matrix) and return it
        sample["hand"] = hand_pc
        sample["joints"] = joints
        sample["obb"] = obb
        sample["trans_mtx"] = trans_mtx
        return sample


class ProjectPointCloud:
    """Callable class that projects the point cloud onto the three OBB planes and
    returns the three projections, together with the new joints' coordinates.

    Attributes:
        res: integer representing the resolution of the (squared) hand projections."""

    def __init__(self, res: int) -> None:
        self.res = res

    def __call__(
        self,
        sample: th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]]:
        # Extract hand point cloud, joints and obb from sample
        hand_pc = sample["hand"]
        joints = sample["joints"]
        obb = sample["obb"]
        joints_pc = np.array(list(joints.values()))

        # Define padding
        pad = int(self.res * 0.1)

        # Compute OBB dimensions
        x_len = obb[4, 0] - obb[0, 0]
        y_len = obb[2, 1] - obb[0, 1]
        z_len = obb[1, 2] - obb[0, 2]

        # Output 3D arrays (CHW order)
        hand_proj = np.ones(shape=(3, self.res, self.res), dtype=np.float32)  # for hand
        joints_proj = np.empty(
            shape=(3, len(joints.keys()), 2), dtype=np.int
        )  # for joints

        # Output projection parameters (5):
        # - coordinates of ROI's upper-left corner (2);
        # - ROI's width and height (2);
        # - projection's scaling factor (1).
        proj_params = np.empty(shape=(3, 5), dtype=np.float32)

        # 1. XY plane

        # Compute orientation
        if x_len >= y_len:
            proj_width = self.res - pad * 2  # apply padding to both ends
            proj_height = round(proj_width * y_len / x_len)
            proj_scale = proj_width / x_len
        else:
            proj_height = self.res - pad * 2  # apply padding to both ends
            proj_width = round(proj_height * x_len / y_len)
            proj_scale = proj_height / y_len

        # Define ROI
        roi = np.ones((proj_height, proj_width))

        # Compute 2D coordinates and clip them
        proj_u = np.rint(proj_scale * (hand_pc[:, 0] + x_len / 2)).astype(np.int)
        proj_v = np.rint(proj_scale * (-hand_pc[:, 1] + y_len / 2)).astype(np.int)
        proj_u = np.clip(proj_u, a_min=0, a_max=proj_width - 1)
        proj_v = np.clip(proj_v, a_min=0, a_max=proj_height - 1)

        # Compute normalized depth and clip it
        depth = (-hand_pc[:, 2] + z_len / 2) / z_len
        depth = np.clip(depth, a_min=0, a_max=1)
        # Set pixel value to depth (keep nearest points in case of overlapping)
        for u, v, d in zip(proj_u, proj_v, depth):
            roi[v, u] = min(d, roi[v, u])

        # Compute ROI upper-left corner
        proj_x = (self.res - proj_width) // 2
        proj_y = (self.res - proj_height) // 2
        # Copy ROI into the whole projection image
        hand_proj[0, proj_y:proj_y + proj_height, proj_x:proj_x + proj_width] = roi

        # Refine projection:
        # 1. Apply opening
        hand_proj[0] = cv2.morphologyEx(
            hand_proj[0],
            cv2.MORPH_OPEN,
            kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2)),
        )
        # 2. Apply median filter
        hand_proj[0] = cv2.medianBlur(hand_proj[0], ksize=5)

        # Project joints' coordinates and clip them
        joints_proj[0, :, 0] = (
            np.rint(proj_scale * (joints_pc[:, 0] + x_len / 2)).astype(np.int) + proj_x
        )
        joints_proj[0, :, 1] = (
            np.rint(proj_scale * (-joints_pc[:, 1] + y_len / 2)).astype(np.int) + proj_y
        )
        joints_proj[0] = np.clip(joints_proj[0], a_min=0, a_max=self.res - 1)

        # Store parameters
        proj_params[0] = np.array([proj_x, proj_y, proj_width, proj_height, proj_scale])

        # 2. YZ plane

        # Compute orientation
        if y_len >= z_len:
            proj_width = self.res - pad * 2  # apply padding to both ends
            proj_height = round(proj_width * z_len / y_len)
            proj_scale = proj_width / y_len
        else:
            proj_height = self.res - pad * 2  # apply padding to both ends
            proj_width = round(proj_height * y_len / z_len)
            proj_scale = proj_height / z_len

        # Define ROI
        roi = np.ones((proj_height, proj_width))

        # Compute 2D coordinates and clip them
        proj_u = np.rint(proj_scale * (-hand_pc[:, 1] + y_len / 2)).astype(np.int)
        proj_v = np.rint(proj_scale * (hand_pc[:, 2] + z_len / 2)).astype(np.int)
        proj_u = np.clip(proj_u, a_min=0, a_max=proj_width - 1)
        proj_v = np.clip(proj_v, a_min=0, a_max=proj_height - 1)

        # Compute normalized depth and clip it
        depth = (-hand_pc[:, 0] + x_len / 2) / x_len
        depth = np.clip(depth, a_min=0, a_max=1)
        # Set pixel value to depth (keep nearest points in case of overlapping)
        for u, v, d in zip(proj_u, proj_v, depth):
            roi[v, u] = min(d, roi[v, u])

        # Compute ROI upper-left corner
        proj_x = (self.res - proj_width) // 2
        proj_y = (self.res - proj_height) // 2
        # Copy ROI into the whole projection image
        hand_proj[1, proj_y:proj_y + proj_height, proj_x:proj_x + proj_width] = roi

        # Refine projection:
        # 1. Apply opening
        hand_proj[1] = cv2.morphologyEx(
            hand_proj[1],
            cv2.MORPH_OPEN,
            kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2)),
        )
        # 2. Apply median filter
        hand_proj[1] = cv2.medianBlur(hand_proj[1], ksize=5)

        # Project joints' coordinates and clip them
        joints_proj[1, :, 0] = (
            np.rint(proj_scale * (-joints_pc[:, 1] + y_len / 2)).astype(np.int) + proj_x
        )
        joints_proj[1, :, 1] = (
            np.rint(proj_scale * (joints_pc[:, 2] + z_len / 2)).astype(np.int) + proj_y
        )
        joints_proj[1] = np.clip(joints_proj[1], a_min=0, a_max=self.res - 1)

        # Store parameters
        proj_params[1] = np.array([proj_x, proj_y, proj_width, proj_height, proj_scale])

        # 3. ZX plane

        # Compute orientation
        if z_len >= x_len:
            proj_width = self.res - pad * 2  # apply padding to both ends
            proj_height = round(proj_width * x_len / z_len)
            proj_scale = proj_width / z_len
        else:
            proj_height = self.res - pad * 2  # apply padding to both ends
            proj_width = round(proj_height * z_len / x_len)
            proj_scale = proj_height / x_len

        # Define ROI
        roi = np.ones((proj_height, proj_width))

        # Compute 2D coordinates and clip them
        proj_u = np.rint(proj_scale * (hand_pc[:, 2] + z_len / 2)).astype(np.int)
        proj_v = np.rint(proj_scale * (hand_pc[:, 0] + x_len / 2)).astype(np.int)
        proj_u = np.clip(proj_u, a_min=0, a_max=proj_width - 1)
        proj_v = np.clip(proj_v, a_min=0, a_max=proj_height - 1)

        # Compute normalized depth and clip it
        depth = (hand_pc[:, 1] + y_len / 2) / y_len
        depth = np.clip(depth, a_min=0, a_max=1)
        # Set pixel value to depth (keep nearest points in case of overlapping)
        for u, v, d in zip(proj_u, proj_v, depth):
            roi[v, u] = min(d, roi[v, u])

        # Compute ROI upper-left corner
        proj_x = (self.res - proj_width) // 2
        proj_y = (self.res - proj_height) // 2
        # Copy ROI into the whole projection image
        hand_proj[2, proj_y:proj_y + proj_height, proj_x:proj_x + proj_width] = roi

        # Refine projection:
        # 1. Apply opening
        hand_proj[2] = cv2.morphologyEx(
            hand_proj[2],
            cv2.MORPH_OPEN,
            kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2)),
        )
        # 2. Apply median filter
        hand_proj[2] = cv2.medianBlur(hand_proj[2], ksize=5)

        # Project joints' coordinates and clip them
        joints_proj[2, :, 0] = (
            np.rint(proj_scale * (joints_pc[:, 2] + z_len / 2)).astype(np.int) + proj_x
        )
        joints_proj[2, :, 1] = (
            np.rint(proj_scale * (joints_pc[:, 0] + x_len / 2)).astype(np.int) + proj_y
        )
        joints_proj[2] = np.clip(joints_proj[2], a_min=0, a_max=self.res - 1)

        # Store parameters
        proj_params[2] = np.array([proj_x, proj_y, proj_width, proj_height, proj_scale])

        # Update dictionary and return it
        sample["hand_proj"] = hand_proj
        sample["joints_proj"] = joints_proj
        sample["proj_params"] = proj_params
        return sample


class LocalContrastNormalization:
    """Callable class that applies Local Contrast Normalization to each projection.

    Attributes:
        _kernel: NumPy's array representing the Gaussian kernel to be applied to the images."""

    def __init__(self, kernel_size: int = 9, sigma: float = 0.8) -> None:
        self._kernel = cv2.getGaussianKernel(ksize=kernel_size, sigma=sigma)

    def __call__(
        self,
        sample: th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]]:
        # Extract hand projections from sample
        hand_projections = sample["hand_proj"]

        # Apply LCN to each projection
        hand_projections_lcn = np.empty(shape=hand_projections.shape)
        for i in range(3):
            x = hand_projections[i]

            # Apply subtractive normalization
            v = x - cv2.filter2D(x, -1, self._kernel)

            # Apply divisive normalization
            sigma = cv2.pow(
                cv2.filter2D(v ** 2, -1, self._kernel),
                0.5,
            )
            c = sigma.mean()
            den = np.maximum(sigma, c)
            den = np.clip(
                den, a_min=1e-15, a_max=None
            )  # clip to epsilon to avoid division by zero

            # Compute actual LCN
            y = v / den

            # Normalize image between 0 and 1
            hand_projections_lcn[i] = cv2.normalize(
                y, dst=None, alpha=0.0, beta=1.0, norm_type=cv2.NORM_MINMAX
            )

        # Update dictionary and return it
        sample["hand_proj"] = hand_projections_lcn
        return sample


class GenerateHeatmaps:
    """Callable class that generate heatmaps from 2D joints coordinates.

    Attributes:
        _proj_res: integer representing the resolution of the (squared) hand projections;
        _heat_res: integer representing the resolution of the (squared) joints heatmaps."""

    def __init__(self, proj_res: int, heat_res: int) -> None:
        self._proj_res = proj_res
        self._heat_res = heat_res

    def __call__(
        self,
        sample: th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]]:
        # Extract joints projections from sample
        joints_projections = sample["joints_proj"]

        n_joints = joints_projections.shape[1]
        joints_heatmaps = np.empty(shape=(3, n_joints, self._heat_res, self._heat_res))
        for i in range(3):
            for j in range(n_joints):
                # Get mean values
                x0 = joints_projections[i, j, 0] * self._heat_res / self._proj_res
                y0 = joints_projections[i, j, 1] * self._heat_res / self._proj_res
                # Define sigma
                sigma = 2
                # Produce ranges for image
                x = np.arange(self._heat_res)
                y = np.arange(self._heat_res)
                # Produce 1D Gaussian distributions
                gx = np.exp(-((x - x0) ** 2) / (2 * sigma ** 2))
                gy = np.exp(-((y - y0) ** 2) / (2 * sigma ** 2))
                # Combine them to obtain a 2D Gaussian distribution
                g = np.outer(gy, gx)
                # Normalize heatmap such that it sums up to 1
                joints_heatmaps[i, j] = g / np.sum(g)

        # Update dictionary and return it
        sample["joints_proj"] = joints_heatmaps
        return sample


class ToTensor:
    """Callable class that turns a NumPy's array sample into a PyTorch's tensor."""

    def __call__(
        self,
        sample: th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray]]],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray], torch.Tensor]]:
        # Extract hand and joints data
        hand_proj = sample["hand_proj"]
        joints_proj = sample["joints_proj"]

        hand_proj = torch.tensor(hand_proj, dtype=torch.float32)
        joints_proj = torch.tensor(joints_proj, dtype=torch.float32)

        # Update dictionary and return it
        sample["hand_proj"] = hand_proj
        sample["joints_proj"] = joints_proj
        return sample


class RandomRotate:
    """Callable class that randomly rotates both hand projections and joints heatmaps."""

    def __call__(
        self,
        sample: th.Dict[
            str, th.Union[np.ndarray, th.Dict[str, np.ndarray], torch.Tensor]
        ],
    ) -> th.Dict[str, th.Union[np.ndarray, th.Dict[str, np.ndarray], torch.Tensor]]:
        # Extract hand and joints data
        hand_proj = sample["hand_proj"]
        joints_proj = sample["joints_proj"]

        # Generate random number between 0 and 3
        k = torch.randint(low=0, high=4, size=()).item()
        # Rotate both hand projections and joints heatmaps
        hand_proj = torch.rot90(hand_proj, k, [1, 2])
        joints_proj = torch.rot90(joints_proj, k, [2, 3])

        # Update dictionary and return it
        sample["hand_proj"] = hand_proj
        sample["joints_proj"] = joints_proj
        return sample
