import os
import random
import time
import typing as th

import numpy as np
import torch
from torch.utils.data import DataLoader

from .dataset import MSRADataset
from .fusion import Fuser
from .nn import MVCNN


def fix_reproducibility(seed: int) -> None:
    """Set deterministic behaviour in PyTorch and fix seed.
    :param seed: the seed to fix reproducibility."""
    # Set seeds for random module, NumPy and Torch
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)

    # Enable CuBLAS deterministic behaviour
    os.environ["CUBLAS_WORKSPACE_CONFIG"] = ":16:8"
    torch.use_deterministic_algorithms(True)

    # Enable cuDNN backend deterministic behaviour
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def compute_error(
    model: MVCNN,
    fuser: Fuser,
    data: MSRADataset,
    indices: th.List[int],
    threshold: float,
    device: torch.device,
) -> np.ndarray:
    """Function that, given a DataLoader, a trained MVCNN model and a Fuser object, performs a heatmap prediction
    and a subsequent Multi-View Fusion to estimate the 3D coordinates of each joint, computes the L2 error w.r.t.
    the ground truth and prints it.
    :param model: the trained MVCNN model;
    :param fuser: the Fuser object which performs the Multi-View Fusion step;
    :param data: the MSRADataset instance;
    :param indices: the indices of the sample to test;
    :param threshold: maximum allowed error to consider a sample as good;
    :param device: device on which the computation is performed.

    :returns a NumPy (n_samples, n_joints) array containing the error for each joint and sample."""

    # Get joints names
    joints_names = data[indices[0]]["joints"].keys()

    n_samples = len(indices)

    # Output (n_samples, n_joints) array
    joints_err = np.empty(shape=(n_samples, len(joints_names)), dtype=np.float64)

    start = time.time()

    # Iterate over batches
    for i, sample_idx in enumerate(indices):
        sample = data[sample_idx]
        # Extract hand projections and move them to device
        hand_proj = sample["hand_proj"].to(device)
        # Feed them to the neural network and move predicted heatmaps to CPU
        sample["pred_heat"] = model.predict(hand_proj).cpu().numpy()

        # Perform Multi-View Fusion
        pred_joints = fuser(sample)

        # Convert ground truth to array
        ground_truth = np.array([c for c in sample["joints"].values()])
        # Compute L2 error for each joint
        joints_err[i] = np.linalg.norm(ground_truth - pred_joints, axis=1)

        print("\r", end="")
        print(f"Progress: {i + 1}/{n_samples}", end="", flush=True)

    elapsed = time.time() - start
    print(f" (elapsed: {elapsed:.1f} s)")

    # Convert to NumPy (n_samples, n_joints) array
    joints_err = np.array(joints_err)

    # Compute average error for each joint
    avg_joints_err = joints_err.mean(axis=1)

    # Compute max error for each sample
    max_joints_err = joints_err.max(axis=1)
    # Count number of samples with max error below threshold
    good_samples = np.count_nonzero(max_joints_err <= threshold)

    print("=" * 50)
    for i, joint_name in enumerate(joints_names):
        print(f"Average error for {joint_name}: {avg_joints_err[i]:.2f} mm")
    print("-" * 50)
    print(f"Average global error: {joints_err.mean():.2f} mm")
    print(f"Proportion of good test samples: {good_samples / joints_err.shape[0]:.2%}")
    print("=" * 50)

    return joints_err
