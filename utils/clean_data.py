#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This script cleans the dataset by removing invalid samples, i.e. samples with less than 100 depth pixels.
Such samples were identified by manually checking the number of non-zero depth pixels,
and stored in the INV_SAMPLES dictionary.
"""

import os

ROOT_FOLDER = os.path.join("..", "data", "cvpr15_MSRAHandGestureDB")
INV_SAMPLES = {
    "P3": {
        "3": [138, 289],
        "4": [442, 443, 445, 446, 447, 448, 449, 450, 451, 452]
    },
    "P4": {
        "IP": [437, 438, 454],
        "RP": [216],
        "T": [9, 45, 46, 89, 90, 91]
    },
    "P5": {
        "7": [16]
    },
    "P6": {
        "4": [212]
    },
    "P8": {
        "4": [168],
        "5": [34]
    }
}


def main():
    for sub in INV_SAMPLES.keys():
        for ges, samples in INV_SAMPLES[sub].items():
            # Rename the binary file of invalid samples
            for sample in samples:
                sample_path = os.path.join(ROOT_FOLDER, sub, ges, f"{sample:06d}_depth.bin")
                os.rename(sample_path, sample_path + ".bak")
            # Rename the joint file
            joint_path = os.path.join(ROOT_FOLDER, sub, ges, "joint.txt")
            os.rename(joint_path, joint_path + ".bak")
            # Discard the lines corresponding to such samples
            with open(joint_path + ".bak", "r") as f:
                lines = f.readlines()
                lines.pop(0)  # discard first line
                lines = [v for i, v in enumerate(lines) if i not in samples]
            # Create a new file with only the valid joints
            with open(joint_path, "w") as f:
                f.write(f"{len(lines)}\n")
                f.writelines(lines)


if __name__ == "__main__":
    main()
