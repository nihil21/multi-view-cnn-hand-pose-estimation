# Create data directory
mkdir -p ../data

# Download zip containing dataset
wget -P ../data https://www.dropbox.com/s/c91xvevra867m6t/cvpr15_MSRAHandGestureDB.zip

# Unzip dataset and remove zip
cd ../data
unzip cvpr15_MSRAHandGestureDB.zip
rm cvpr15_MSRAHandGestureDB.zip
